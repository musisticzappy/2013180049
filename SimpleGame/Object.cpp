#include "stdafx.h"
#include "Object.h"
#include <float.h>
#include <math.h>

#define		gravity 0.98
Object::Object()
{
}

Object::~Object()
{
}

void Object::Update(float eTime)
{
	m_fLife += eTime;

	float velocity = sqrtf(m_fVelX * m_fVelX + m_fVelY * m_fVelY);		// size of velecity
	if (velocity < FLT_EPSILON)
	{

	}
	else
	{
		// gravity force
		float gZ;
		gZ = 9.8 * m_fMass;		// 수직항력
		float friction;
		friction = m_FrictionCoef * gZ;		// 마찰력의 크기

		// friction
		float fX, fY, size;
		fX = -friction*m_fVelX / velocity;
		fY = -friction*m_fVelY / velocity;

		// calc acc
		float accX = fX / m_fMass;
		float accY = fY / m_fMass;

		float afterVelX = m_fVelX + eTime * accX;
		float afterVelY = m_fVelY + eTime * accY;

		if (afterVelX * m_fVelX < 0.f)
		{
			m_fVelX = 0.f;
		}
		else
		{
			m_fVelX += eTime * accX;
		}

		if (afterVelY * m_fVelY < 0.f)
		{
			m_fVelY = 0.f;
		}
		else
		{
			m_fVelY += eTime * accY;
		}
		
	}

	// Calc Velocity
	m_fVelX += eTime * m_fAccX;
	m_fVelY += eTime * m_fAccY;

	// Calc Position
	m_fPosX += eTime * m_fVelX;
	m_fPosY += eTime * m_fVelY;
	
	m_fPosHeight = m_fHeightTest + sin(m_fLife * 3.f) * 0.2f;
}

void Object::KeyInput(unsigned char key, int x, int y)
{
	//if (key == 'w')
	//{
	//	m_fPosY += (1.f / 60.f) * m_fVelY;
	//}
	//if (key == 's')
	//{
	//	m_fPosY -= (1.f / 60.f) * m_fVelY;
	//}
	if (key == 'a')
	{
		m_fPosX -= (1.f / 60.f) * m_fVelX;
	}
	if (key == 'd')
	{
		m_fPosX += (1.f / 60.f) * m_fVelX;
	}
}

void Object::ApplyForce(float fX, float fY, float eTime)
{
	// Calc acc
	m_fAccX = fX / m_fMass;
	m_fAccY = fY / m_fMass;

	// Calc Velocity
	m_fVelX += eTime * m_fAccX;
	m_fVelY += eTime * m_fAccY;

	m_fAccX = 0.f;
	m_fAccY = 0.f;
}
