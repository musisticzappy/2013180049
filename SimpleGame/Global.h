#pragma once

#define HERO_ID 0

#define MAX_OBJECTS 300

#define SHOOT_NONE 0
#define SHOOT_UP 1
#define SHOOT_DOWN 2
#define SHOOT_LEFT 3
#define SHOOT_RIGHT 4

#define KIND_HERO 0
#define KIND_BULLET 1