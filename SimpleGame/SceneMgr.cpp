#include "stdafx.h"
#include "SceneMgr.h"
#include "Renderer.h"
#include "Object.h"
default_random_engine dre;
uniform_real_distribution<float> uid(-2.7f, 2.7f);

SceneMgr::SceneMgr()
	: m_pRenderer(nullptr)
{
	enemyTime = (float)timeGetTime() * 0.001f;
	startTime = (float)timeGetTime() * 0.001f;
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		m_pObjects[i] = nullptr;
	}
	Initialize();
}


SceneMgr::~SceneMgr()
{
	//m_pRenderer->DeleteTexture(m_iObjTexIdx);

	delete m_pRenderer;
	m_pRenderer = nullptr;

	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		DeleteObject(i);
	}
}

void SceneMgr::Initialize()
{
	m_pRenderer = new Renderer(500, 500);
	if (!m_pRenderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//m_iObjTexIdx = m_pRenderer->CreatePngTexture("./Texture/Charactervector.png");
	m_TexSeq = m_pRenderer->CreatePngTexture("./Texture/Charactervector.png");
	m_TexBullet = m_pRenderer->CreatePngTexture("./Texture/bullet.png");
	m_TexHP = m_pRenderer->CreatePngTexture("./Texture/HP.png");
	m_TexSTART = m_pRenderer->CreatePngTexture("./Texture/start.png");
	m_TexOVER = m_pRenderer->CreatePngTexture("./Texture/GAMEOVER.png");

	m_pObjects[HERO_ID] = new Object();
	m_pObjects[HERO_ID]->SetPos(0.f, -1.8f, 0.7f);
	m_pObjects[HERO_ID]->SetSize(0.3f, 0.3f);
	m_pObjects[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_pObjects[HERO_ID]->SetVel(0.f, 0.f);
	m_pObjects[HERO_ID]->SetAcc(0.f, 0.f);
	m_pObjects[HERO_ID]->SetMass(0.3f);
	m_pObjects[HERO_ID]->SetFrictionCoef(0.05f);
	m_pObjects[HERO_ID]->SetKind(KIND_HERO);

	m_Sound = new Sound();
}

float temp = 0.f;

void SceneMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.6f, 0.9f, 1.0f, 1.0f);

	// Renderer Test
	float x, y, posHeight;
	m_pObjects[HERO_ID]->GetPos(&x, &y, &posHeight);

	float width, height;
	m_pObjects[HERO_ID]->GetSize(&width, &height);
	float r, g, b, a;
	m_pObjects[HERO_ID]->GetColor(&r, &g, &b, &a);
	
	float newX, newY, newPosHeight, newW, newH;
	newX = x * 100.f;
	newY = y * 100.f;
	newPosHeight = posHeight * 100.f;
	newW = width * 100.f;
	newH = height * 100.f;

	//m_pRenderer->DrawSolidRect(newX, newY, 0, newW, newH, r, g, b, a);
	//m_pRenderer->DrawTextureRect(newX, newY, 0, newW, newH, r, g, b, a, m_iObjTexIdx);
	//m_pRenderer->DrawTextureRectHeight(newX, newY, 0, newW, newH, r, g, b, a, m_iObjTexIdx, newPosHeight);
	m_pRenderer->DrawTextureRectSeqXY(newX, newY, 0, newW, newH, r, g, b, a, m_TexSeq, m_curX, m_curY, 4, 4);

	for (int i = 0; i < hpCount; ++i) {
		m_pRenderer->DrawTextureRectSeqXY(-220 + (hpCount + (i * 30)), -220, 0, 50, 50, r, g, b, a, m_TexHP, 10, 10, 1, 1);
	}
	float NowTime = (float)timeGetTime() * 0.001f;
	if (NowTime - startTime <= 2.0f)
		m_pRenderer->DrawTextureRectSeqXY(0, 50, 0, 500, 500, r, g, b, a, m_TexSTART, 10, 10, 1, 1);

	if (gameover == TRUE)
		m_pRenderer->DrawTextureRectSeqXY(0, 50, 0, 500, 500, r, g, b, a, m_TexOVER, 10, 10, 1, 1);

	for (int i = 1; i < MAX_OBJECTS; ++i)
	{
		if (m_pObjects[i] != nullptr)
		{
			float x, y, posHeight;
			m_pObjects[i]->GetPos(&x, &y, &posHeight);

			float width, height;
			m_pObjects[i]->GetSize(&width, &height);
			float r, g, b, a;
			m_pObjects[i]->GetColor(&r, &g, &b, &a);

			float newX, newY, newPosHeight, newW, newH;
			newX = x * 100.f;
			newY = y * 100.f;
			newPosHeight = posHeight * 100.f;
			newW = width * 100.f;
			newH = height * 100.f;

		
			static int g_Seq = 0;
			int seqX = g_Seq % 9;
			int seqY = (int)(g_Seq / 9.f);
			g_Seq++;
			if (g_Seq > 80)
				g_Seq = 0;
				
		
			m_pRenderer->DrawTextureRectSeqXY(newX, newY, 0, newW, newH, r, g, b, a, m_TexBullet, m_curX, m_curY, 1, 1);
		}
	}
}


void SceneMgr::Update(float fDelta)
{
	int index = m_Sound->CreateSound("ophelia.mp3");
	m_Sound->PlaySound(index, 1, 7);
	m_pObjects[HERO_ID]->Update(fDelta);
	if (gameover == FALSE) {

		float NowTime = (float)timeGetTime() * 0.001f;
		if (NowTime - enemyTime >= 0.3f)
		{
			if (enemyNum <= MAX_OBJECTS)
				AddObject(2.0f, 2.7f, 0.7f, 0.f, 0.f, 0.2f, -0.2f, 0.2f, 1.f);

			enemyTime = NowTime;
		}
		for (int i = 1; i < MAX_OBJECTS; ++i) {
			if (m_pObjects[i] != nullptr) {
				//m_pObjects[i]->Update(fDelta);

				m_pObjects[i]->SetPos(m_pObjects[i]->GetPosX(), m_pObjects[i]->GetPosY() - 0.03f, 0.7f);
			}
		}

		m_curX += fDelta * 10.f;
		if (m_curX > 5.f)
		{
			m_curX = 0.f;

		}

		for (int i = 1; i < MAX_OBJECTS; ++i)
			if (m_pObjects[i] != nullptr) {
				if (m_pObjects[i]->GetPosY() < -2.7f)
					m_pObjects[i]->SetPos(m_pObjects[i]->GetPosX(), 2.7f, 0.7f);
			}

	}
	CheckCol();
}

void SceneMgr::KeyInput(unsigned char key, int x, int y)
{
	//m_pObjects[0]->KeyInput(key, x, y);
	//if (key == 'w')
	//{
	//	float x, y;
	//	m_pObjects[0]->GetPos(&x, &y);
	//	m_pObjects[0]->SetPos(x, y + 5.f);
	//}
	//if (key == 's')
	//{
	//	float x, y;
	//	m_pObjects[0]->GetPos(&x, &y);
	//	m_pObjects[0]->SetPos(x, y - 5.f);
	//}
	//if (key == 'a')
	//{
	//	float x, y;
	//	m_pObjects[0]->GetPos(&x, &y);
	//	m_pObjects[0]->SetPos(x - 5.f, y);
	//}
	//if (key == 'd')
	//{
	//	float x, y;
	//	m_pObjects[0]->GetPos(&x, &y);
	//	m_pObjects[0]->SetPos(x + 5.f, y);
	//}
}

void SceneMgr::ApplyForce(float fX, float fY, float eTime)
{
	m_pObjects[HERO_ID]->ApplyForce(fX, fY, eTime);
}

int SceneMgr::AddObject(float posX, float posY, float height, float velX, float velY, float sizeX, float sizeY, float mass, float friction)
{
	posX = uid(dre);

	int idx = FindEmptySlot();
	if (idx < 0)
		return -1;

	m_pObjects[idx] = new Object();
	m_pObjects[idx]->SetPos(posX, posY, height);
	m_pObjects[idx]->SetSize(sizeX, sizeY);
	m_pObjects[idx]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_pObjects[idx]->SetVel(velX, velY);
	m_pObjects[idx]->SetAcc(0.f, 0.f);
	m_pObjects[idx]->SetMass(mass);
	m_pObjects[idx]->SetFrictionCoef(friction);

	return idx;
}

void SceneMgr::DeleteObject(unsigned int idx)
{
	if (idx > MAX_OBJECTS)
		return;
	if (m_pObjects[idx])
	{
		delete(m_pObjects[idx]);
		m_pObjects[idx] = nullptr;
	}
}

int SceneMgr::FindEmptySlot()
{
	int idx = -1;
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_pObjects[i] == nullptr)
		{
			idx = i;
			break;
		}
			
	}
	if (idx == -1)
		std::cout << "All Slots are occupied." << std::endl;

	return idx;
}

void SceneMgr::Shoot(int dir)
{
	/*if (dir == SHOOT_NONE)
		return;

	float posX, posY, height, velX, velY;
	m_pObjects[HERO_ID]->GetPos(&posX, &posY, &height);
	m_pObjects[HERO_ID]->GetVel(&velX, &velY);
	float bvX = 0.f, bvY = 0.f;
	float amount = 10.f;
	switch (dir)
	{
	case SHOOT_UP:
		bvX = 0;
		bvY = amount;
		break;
	case SHOOT_DOWN:
		bvX = 0;
		bvY = -amount;
		break;
	case SHOOT_LEFT:
		bvX = -amount;
		bvY = 0;
		break;
	case SHOOT_RIGHT:
		bvX = amount;
		bvY = 0;
		break;
	}

	AddObject(posX, posY, height, velX+ bvX, velY+ bvY, 0.3f, 0.3f, 0.1f, 0.01f);*/
}

void SceneMgr::CheckCol()
{
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_pObjects[i] == nullptr)
			continue;

		int collisionCount = 0;
		for (int j = 0; j < MAX_OBJECTS; ++j)
		{
			if (m_pObjects[j] == nullptr)
				continue;
			if (i == j)
				continue;
			float posX, posY, height, sizeX, sizeY;
			m_pObjects[i]->GetPos(&posX, &posY, &height);
			m_pObjects[i]->GetSize(&sizeX, &sizeY);

			float minX1, minY1, maxX1, maxY1;
			minX1 = posX - sizeX / 3.f;
			minY1 = posY - sizeX / 3.f;
			maxX1 = posX + sizeX / 3.f;
			maxY1 = posY + sizeX / 3.f;
			float pX, pY, h, sX, sY;
			m_pObjects[j]->GetPos(&pX, &pY, &h);
			m_pObjects[j]->GetSize(&sX, &sY);
			if (sY < 0)
				sY *= -1;
			float minX2, minY2, maxX2, maxY2;
			minX2 = pX - sX / 3.f; maxX2 = pX + sX / 3.f;
			minY2 = pY - sY / 3.f; maxY2 = pY + sY / 3.f;
			if (RRCollision(minX1, minY1, maxX1, maxY1, minX2, minY2, maxX2, maxY2))
			{
				collisionCount++;
			}

		}
		if (collisionCount <= 0)
		{
			m_pObjects[i]->SetColor(1.f, 1.f, 1.f, 1.f);
		}
		else  {
			gameover = TRUE;
			int index = m_Sound->CreateSound("explosion.wav");
			m_Sound->PlaySound(index, 1, 7);
			m_pObjects[i]->SetColor(0.9f, 0.f, 0.f, 0.0f);
		}

	}
}

bool SceneMgr::RRCollision(float minX1, float minY1, float maxX1, float maxY1, float minX2, float minY2, float maxX2, float maxY2)
{
	if (minX1 > maxX2)
		return false;
	if (maxX1 < minX2)
		return false;

	if (minY1 > maxY2)
		return false;
	if (maxY1 < minY2)
		return false;

	return true;
}
