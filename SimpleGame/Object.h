#pragma once
class Object
{
public:
	Object();
	//Object(float fPosX, float fPosY, float fWidth, float fHeight, float fR, float fG, float fB, float fA);
	~Object();

public:
	void		Update(float eTime);
	void		KeyInput(unsigned char key, int x, int y);
	bool        Alive = TRUE;
private:
	float		m_fPosX;
	float		m_fPosY;
	float		m_fPosHeight;
	float		m_fWidth;
	float		m_fHeight;
	float		m_fR;
	float		m_fG;
	float		m_fB;
	float		m_fA;

	float		m_fVelX;
	float		m_fVelY;
	// > 한 픽셀 = 1cm

	float		m_fAccX;		// > 가속도
	float		m_fAccY;

	float		m_fMass;		// > 질량

	float		m_fRough;		// > 마찰(거친정도)
	float		m_FrictionCoef;	// > 마찰계수

	float		m_fLife = 0.f;		// > 생성된 이후 지난 시간
	float		m_fHeightTest;

	int			m_Kind;


public:
	void	SetPos(float fX, float fY, float height) { m_fPosX = fX; m_fPosY = fY; m_fPosHeight = m_fHeightTest = height; }
	void	SetSize(float fWidth, float fHeight) { m_fWidth = fWidth; m_fHeight = fHeight; }
	void	SetColor(float fR, float fG, float fB, float fA) { m_fR = fR; m_fG = fG; m_fB = fB; m_fA = fA; };
	void	SetVel(float x, float y) { m_fVelX = x; m_fVelY = y; }
	void	SetAcc(float x, float y) { m_fAccX = x; m_fAccY = y; }
	void	SetMass(float mass) { m_fMass = mass; }
	void	SetRough(float rough) { m_fRough = rough; }
	void	SetFrictionCoef(float x) { m_FrictionCoef = x; }
	void	SetKind(int kind) { m_Kind = kind; }


	float	GetPosY() { return m_fPosY; }
	float	GetPosX() { return m_fPosX; }
	void	GetPos(float* fX, float* fY, float* height) { *fX = m_fPosX; *fY = m_fPosY; *height = m_fPosHeight; }
	void	GetSize(float* fWidth, float* fHeight) { *fWidth = m_fWidth; *fHeight = m_fHeight; }
	void	GetColor(float* fR, float* fG, float* fB, float* fA) { *fR = m_fR; *fG = m_fG; *fB = m_fB; *fA = m_fA; }
	void	GetVel(float* x, float* y) { *x = m_fVelX; *y = m_fVelY; }
	void	GetAcc(float* x, float* y) { *x = m_fAccX; *y = m_fAccY; }
	void	GetMass(float* mass) { *mass = m_fMass; }
	void	GetRough(float* rough) { *rough = m_fRough; }
	void	GetFrictionCoef(float* x) { *x = m_FrictionCoef; }
	void	GetKind(int* kind) { *kind = m_Kind; }

	void	ApplyForce(float fX, float fY, float eTime);
};

// > ApplyForce(float fX, float fY)
// > 유저 입력마다 불리는 함수
// > 힘을 N단위로 넘김
// > 넘겨받은 힘과 시간을 기준으로 Object 에 가속도 적용