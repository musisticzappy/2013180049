/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "SceneMgr.h"

//float fTime = 0.f;
SceneMgr*		g_SceneMgr;
DWORD			g_PrevTime = 0;

bool			g_KeyW = false;
bool			g_KeyS = false;
bool			g_KeyA = false;
bool			g_KeyD = false;

int				g_Shoot = 0;

float			g_ForceX = 0.f;
float			g_ForceY = 0.f;

void RenderScene(void)
{
	if (g_PrevTime == 0)
	{
		g_PrevTime = timeGetTime();
	}
	DWORD curtime = timeGetTime();
	DWORD elapsedTime = curtime - g_PrevTime;
	float eTime = (float)elapsedTime / 1000.f;	// > 1000ms = 1s
//	std::cout << "eTime : " << eTime << std::endl;
	g_PrevTime = curtime;


	float fX = 0.f, fY = 0.f;
	float amount = 1.f;
	//if (g_KeyW)
	//	fY += amount;
	//if (g_KeyS)
	//	fY -= amount;
	if (g_KeyA)
		fX -= amount;
	if (g_KeyD)
		fX += amount;

	g_SceneMgr->ApplyForce(fX, fY, eTime);

	g_SceneMgr->Update(eTime);
	g_SceneMgr->RenderScene();
	glutSwapBuffers();

	if (g_Shoot != SHOOT_NONE)
		g_SceneMgr->Shoot(g_Shoot);

	//Normalize(&fx, &fy);


	std::cout << "g_KeyW : " << g_KeyW << ", g_KeyS : " << g_KeyS << ", g_KeyA : " << g_KeyA << ", g_KeyD : " << g_KeyD << std::endl;
}

void Idle(void)
{
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	g_SceneMgr->KeyInput(key, x, y);
	//if (key == 'w')
	//{
	//	g_KeyW = true;
	//	g_ForceY = 1.f;
	//}
	//if (key == 's')
	//{
	//	g_KeyS = true;
	//	g_ForceY = -1.f;
	//}
	if (key == 'a')
	{
		g_KeyA = true;
		g_ForceX = -1.f;
	}
	if (key == 'd')
	{
		g_KeyD = true;
		g_ForceX = 1.f;
	}
	RenderScene();
}

void KeyUpInput(unsigned char key, int x, int y)
{
	g_SceneMgr->KeyInput(key, x, y);
	if (key == 'w')
	{
		g_KeyW = false;
		g_ForceY = 0.f;
	}
	if (key == 's')
	{
		g_KeyS = false;
		g_ForceY = 0.f;
	}
	if (key == 'a')
	{
		g_KeyA = false;
		g_ForceX = 0.f;
	}
	if (key == 'd')
	{
		g_KeyD = false;
		g_ForceX = 0.f;
	}
	RenderScene();
}

void SpecialKeyDownInput(int key, int x, int y)
{
	RenderScene();
	switch (key)
	{
	case GLUT_KEY_UP:
		g_Shoot = SHOOT_UP;
		break;
	case GLUT_KEY_DOWN:
		g_Shoot = SHOOT_DOWN;
		break;
	case GLUT_KEY_LEFT:
		g_Shoot = SHOOT_LEFT;
		break;
	case GLUT_KEY_RIGHT:
		g_Shoot = SHOOT_RIGHT;
		break;
	}
}

void SpecialKeyUpInput(int key, int x, int y)
{
	RenderScene();
	g_Shoot = SHOOT_NONE;
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	// Initialize SceneMgr
	g_SceneMgr = new SceneMgr;

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	glutMainLoop();

	delete g_SceneMgr;

    return 0;
}
