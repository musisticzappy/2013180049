#pragma once

#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "Global.h"
#include "Sound.h"

class Renderer;
class Object;

class SceneMgr
{
public:
	SceneMgr();
	~SceneMgr();

private:
	void		Initialize();

public:
	void		RenderScene();
	void		Update(float fDelta);
	void		KeyInput(unsigned char key, int x, int y);
	void		ApplyForce(float fX, float fY, float eTime);

	int			AddObject(float posX, float posY, float height, float velX, float velY, float sizeX, float sizeY, float mass, float friction);
	void		DeleteObject(unsigned int idx);

	int			FindEmptySlot();

	void		Shoot(int dir);

	void		CheckCol();
	bool		RRCollision(float minX1, float minY1, float maxX1, float maxY1, float minX2, float minY2, float maxX2, float maxY2);

private:
	Renderer*			m_pRenderer;
	Object*				m_pObjects[MAX_OBJECTS];

	//GLuint				m_iObjTexIdx = -1;
	GLuint				m_TexSeq = -1;
	GLuint				m_TexBullet = -1;
	GLuint				m_TexHP = -1;
	GLuint				m_TexSTART = -1;
	GLuint				m_TexOVER = -1;

	float				m_curX = 0.f;
	float				m_curY = 0.f;

	float enemyTime;
	float startTime;

	bool gameover = FALSE;
	int enemyNum;
	int hpCount = 1;

	Sound*			    m_Sound;
};

